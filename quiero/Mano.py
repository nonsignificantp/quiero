class Mano():
    def __init__(self, cartas):
        self.cartas = cartas
    
    def envido(self):
        envidoValor = [11,12,13,14,15,16,17,0,0,10,10,10]
        
        for palo in ['b','c','e','o']:
            pata = [int(carta.valor) for carta in self.cartas if carta.palo == palo]
                
            if len(pata) is 2:
                total = sum([envidoValor[(carta-1)] for carta in pata])              
                return total
        
            if len(pata) is 3:
                pata = sorted(pata, reverse = True)
                total = sum([envidoValor[(carta-1)] for carta in pata[0:2]])
                return total
        
        return False
    
    def flor(self):
        palos = ['b','c','e','o']
        
        for palo in palos:
            if all([carta.palo == palo for carta in self.cartas]):
                return True
    
        return False
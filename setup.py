from setuptools import setup

setup(name='quiero',
      version='0.1',
      description='Tools for Truco analysis',
      url='',
      author='Agustin Rodriguez',
      author_email='gus990@gmail.com',
      license='MIT',
      packages=['quiero'],
      install_requires=['random'],
      zip_safe=False)
class Carta():
    def __init__(self, valor, palo):
        self.valor = int(valor)
        self.palo = palo
        self.posicion = self._poss(self.valor, self.palo)
        
    def __repr__(self):
        index = {'b': 'basto', 'c': 'copa', 'e': 'espada', 'o': 'oro'}
        mask = 'Carta(\'{} de {}\')'.format(self.valor, index[self.palo])
        
        return(mask)
    
    def __str__(self):
        mask = '{}{}'.format(self.valor, self.palo)
        
        return(mask)
    
    def _poss(self, vlr, pl):
        index = ['1e','1b','7e','7o',['3b','3c','3e','3o'],['2b','2c','2e','2o'],['1c','1o'],['12b','12c','12e','12o'],['11b','11c','11e','11o'],['10b','10c','10e','10o'],['7b','7c'],['6b','6c','6e','6o'],['5b','5c','5e','5o'],['4b','4c','4e','4o']]
        
        for poss, carta in enumerate(index):
            if isinstance(carta, list):
                for e in carta:
                    if e == '{}{}'.format(vlr, pl):
                        return poss
            
            if isinstance(carta, str):
                if carta == '{}{}'.format(vlr, pl):
                    return poss
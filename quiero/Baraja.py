from quiero.Carta import Carta
from random import shuffle, sample

class Baraja():
    
    def __init__(self):
        self.cartas = self.crear()
        
    def crear(self):
        valores = [str(n) for n in range(1, 8)] + [str(n) for n in range(10, 13)]
        palos = ['b', 'c', 'e', 'o']
        naipes = [Carta(valor, palo) for palo in palos for valor in valores]
        shuffle(naipes)
        
        return naipes
    
    def repartir(self, jugadores = 2):
        proManos = sample(self.cartas, k = (3 * jugadores))
        metaManos = [[] for x in range(0,jugadores)]

        while proManos:
            for i in range(0,jugadores):
                metaManos[i].append(proManos.pop())

        return metaManos
    
    def __repr__(self):
        return str(self.cartas)
    
    def __str__(self):
        string = [str(x) for x in self.cartas]
        return ' '.join(string)
    
    def __len__(self):
        return len(self.cartas)
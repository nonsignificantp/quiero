
from itertools import permutations
from quiero.constantes import Carta, ordenCartas

def match(ply1, ply2):
    """
    Given two hands, Returns the probability of the first hand winning
    """

    for mano in [ply1, ply2]:
        for i, carta in enumerate(mano):
            mano[i] = _valores(carta)
    
    allPly2 = list(permutations(ply2))
    resultado = []

    for mano in allPly2:
        partido = []
        for carta1, carta2 in zip(ply1, mano):
            if carta1 < carta2:
                partido.append(1)
            else:
                partido.append(0)
        resultado.append(sum(partido))
    
    resultado = [puntaje > 1 for puntaje in resultado]
    prob = resultado.count(True)/len(resultado)
    
    return prob
    
def _valores(carta):
    
    for i, e in enumerate(ordenCartas):
        
        if isinstance(e, list):
            for x in e:
                if carta == x:
                    return i

        if carta  == e:
            return i